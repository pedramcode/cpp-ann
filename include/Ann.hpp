#include <iostream>
#include <vector>
#include "./Matrix.hpp"
#include <math.h>
#include <algorithm>

using namespace std;

class Ann{
    private:
        vector<Matrix<float>> weights;
        vector<Matrix<float>> bias;
        int input_num;
        int output_num;
        float lr;
    public:
        Ann(int *_arch, int _size, float _lr);
        ~Ann();
        vector<Matrix<float>> feedforward(float *_x);
        void predict(float *_x, float* _out_buffer);
        static float relu(float x);
        static float sigmoid(float x);
        static float drv_sigmoid(float x);
        static float tanh(float x);
        static float drv_tanh(float x);
        void display_weights();
        float fit(float *_x, float *_y);
        Matrix<float> mean_squared_error(Matrix<float> _target, Matrix<float> _output,bool drv);
};