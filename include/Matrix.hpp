#include <iostream>
#include <chrono>

using namespace std;

template<class T>
class Matrix{
    private:
        int width;
        int height;
    public:
        T **mat;
        Matrix(int _height, int _width);
        ~Matrix();
        void display();
        void randomize(int max);
        void free();
        static Matrix map(Matrix _matrix, T (*f)(T));
        Matrix copy();
        static Matrix from_array (T *_arr, int _size);
        static Matrix transpose (Matrix _matrix);
        void to_array(T *buffer);
        float sum();
        Matrix operator* (const Matrix _matrix);
        Matrix operator* (T _n);
        Matrix operator+ (const Matrix _matrix);
        Matrix operator+ (T _n);
        Matrix operator- (const Matrix _matrix);
        Matrix operator- (T _n);
        Matrix operator/ (T _n);
        int get_width();
        int get_height();
};