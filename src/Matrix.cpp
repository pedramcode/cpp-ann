#include "../include/Matrix.hpp"

template<class T>
Matrix<T>::Matrix(int _height, int _width){
    this->width = _width;
    this->height = _height;
    this->mat = new T*[_height];
    for (int i = 0; i < _height; i++) {
        mat[i] = new T[_width];
    }
    srand(chrono::system_clock::now().time_since_epoch().count());
}

template<class T>
Matrix<T>::~Matrix(){
}

template<class T>
float Matrix<T>::sum(){
    float s=0;
    for(int i = 0 ; i < height ; i++){
        for(int j = 0 ; j < width ; j++){
            s+=this->mat[i][j];
        }
    }
    return s;
}

template<class T>
Matrix<T> Matrix<T>::transpose(Matrix<T> _matrix){
    Matrix<T> r_mat(_matrix.width,_matrix.height);
    for(int i = 0 ; i < _matrix.height ; i++){
        for(int j = 0 ; j < _matrix.width ; j++){
            r_mat.mat[j][i] = _matrix.mat[i][j];
        }
    }
    return r_mat;
}

template<class T>
void Matrix<T>::to_array(T *buffer){
    for(int i = 0 ; i < height ; i++){
        buffer[i] = mat[i][0];
    }
}

template<class T>
Matrix<T> Matrix<T>::copy(){
    Matrix<T> c_mat(this->height, this->width);
    for(int i = 0 ; i < height ; i++){
        for(int j = 0 ; j < width ; j++){
            c_mat.mat[i][j] = this->mat[i][j];
        }
    }
    return c_mat;
}

template<class T>
void Matrix<T>::display(){
    cout << "[" << endl;
    for(int i = 0 ; i < height ; i++){
        for(int j = 0 ; j < width ; j++){
            if(j==0){
                cout<<"\t[";
            }
            cout << mat[i][j];
            if(j!=width-1){
                cout<<"\t";
            }else{
                cout<<"]"<<endl;
            }
        }
    }
    cout << "];" << endl;
}

template<class T>
void Matrix<T>::randomize(int max){
    for(int i = 0 ; i < height ; i++){
        for(int j = 0 ; j < width ; j++){
            mat[i][j] = rand()%max;
        }
    }
}

template<class T>
void Matrix<T>::free(){
    delete this->mat;
}

template<class T>
Matrix<T> Matrix<T>::map(Matrix<T> _matrix, T (*f)(T)){
    Matrix<T> _res(_matrix.get_height(), _matrix.get_width());
    for(int i = 0 ; i < _matrix.get_height() ; i++){
        for(int j = 0 ; j < _matrix.get_width() ; j++){
            _res.mat[i][j] = f(_matrix.mat[i][j]);
        }
    }
    return _res;
}

template<class T>
Matrix<T> Matrix<T>::from_array(T *_arr, int _size){
    Matrix<T> mat_res(_size, 1);
    for(int i = 0 ; i < _size ; i++){
        mat_res.mat[i][0] = _arr[i];
    }
    return mat_res;
}

template<class T>
Matrix<T> Matrix<T>::operator*(const Matrix<T> _matrix){
    if(_matrix.width == width && _matrix.height == height){
        Matrix<T> mat_res(height, width);
        for(int i = 0 ; i < this->height ; i++){
            for(int j = 0 ; j < this->width ; j++){
                mat_res.mat[i][j] = this->mat[i][j] * _matrix.mat[i][j];
            }
        }
        return mat_res;
    }
    Matrix<T> mat_res(height, _matrix.width);
    for(int k = 0 ; k < _matrix.width ; k++){
        for(int i = 0 ; i < this->height ; i++){
            T sum = 0;
            for(int j = 0 ; j < this->width ; j++){
                sum += this->mat[i][j] * _matrix.mat[j][k];
            }
            mat_res.mat[i][k] = sum;
        }
    }
    return mat_res;
}

template<class T>
Matrix<T> Matrix<T>::operator*(T _n){
    Matrix<T> mat_res(height, width);
    for(int i = 0 ; i < this->height ; i++){
        for(int j = 0 ; j < this->width ; j++){
            mat_res.mat[i][j] = mat[i][j] * _n;
        }
    }
    return mat_res;
}

template<class T>
Matrix<T> Matrix<T>::operator+(T _n){
    Matrix<T> mat_res(height, width);
    for(int i = 0 ; i < this->height ; i++){
        for(int j = 0 ; j < this->width ; j++){
            mat_res.mat[i][j] = mat[i][j] + _n;
        }
    }
    return mat_res;
}

template<class T>
Matrix<T> Matrix<T>::operator+(const Matrix _matrix){
    Matrix<T> mat_res(height, width);
    for(int i = 0 ; i < this->height ; i++){
        for(int j = 0 ; j < this->width ; j++){
            mat_res.mat[i][j] = mat[i][j] + _matrix.mat[i][j];
        }
    }
    return mat_res;
}

template<class T>
Matrix<T> Matrix<T>::operator-(T _n){
    Matrix<T> mat_res(height, width);
    for(int i = 0 ; i < this->height ; i++){
        for(int j = 0 ; j < this->width ; j++){
            mat_res.mat[i][j] = mat[i][j] - _n;
        }
    }
    return mat_res;
}

template<class T>
Matrix<T> Matrix<T>::operator-(const Matrix _matrix){
    Matrix<T> mat_res(height, width);
    for(int i = 0 ; i < this->height ; i++){
        for(int j = 0 ; j < this->width ; j++){
            mat_res.mat[i][j] = mat[i][j] - _matrix.mat[i][j];
        }
    }
    return mat_res;
}

template<class T>
Matrix<T> Matrix<T>::operator/(T _n){
    Matrix<T> mat_res(height, width);
    for(int i = 0 ; i < this->height ; i++){
        for(int j = 0 ; j < this->width ; j++){
            mat_res.mat[i][j] = mat[i][j] / _n;
        }
    }
    return mat_res;
}

template<class T>
int Matrix<T>::get_width(){
    return width;
}

template<class T>
int Matrix<T>::get_height(){
    return height;
}

template class Matrix<float>;
template class Matrix<int>;
template class Matrix<double>;