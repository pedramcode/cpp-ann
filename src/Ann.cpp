#include "../include/Ann.hpp"

Ann::Ann(int *_arch, int _size,float _lr){
    input_num = _arch[0];
    output_num = _arch[_size-1];
    for(int i = 1 ; i < _size ; i++){
        Matrix<float> wt(_arch[i],_arch[i-1]);
        Matrix<float> bs(_arch[i],1);
        wt.randomize(1000);
        bs.randomize(1000);
        wt = wt / 1000;
        bs = bs / 1000;
        this->weights.push_back(wt);
        this->bias.push_back(bs);
    }
    this->lr = _lr;
    
}

Ann::~Ann(){
    for(int i = 1 ; i < weights.size() ; i++){
        weights[i].free();
    }
}

void Ann::display_weights(){
    for(int i = 0 ; i < weights.size(); i++){
        cout<<"Weight (" << i << ")" <<endl;
        weights[i].display();
    }
}

float Ann::relu(float x){
    if(x<0){
        return 0;
    }
    return x;
}

float Ann::sigmoid(float x){
    return 1/(1+exp(-x));
}

float Ann::drv_sigmoid(float x){
    return Ann::sigmoid(x)*(1 - Ann::sigmoid(x));
}

float Ann::tanh(float x){
    return (exp(x)-exp(-x))/(exp(x)+exp(-x));
}

float Ann::drv_tanh(float x){
    return 1 - pow(Ann::tanh(x),2);
}


vector<Matrix<float>> Ann::feedforward(float *_x){
    auto input_mat = Matrix<float>::from_array(_x, input_num);
    vector<Matrix<float>> layers;
    auto tmp_mat = (weights[0] * input_mat) + bias[0];
    
    layers.push_back(tmp_mat);
    auto mapped = Matrix<float>::map(tmp_mat,this->tanh);
    layers.push_back(mapped);
    for(int i = 1 ; i < weights.size() ; i++){
        auto layer_mat = (weights[i] * layers[i-1]) + bias[i];
        layers.push_back(layer_mat);
        auto layer_mapped = Matrix<float>::map(layer_mat,this->tanh);
        layers.push_back(layer_mapped);
    }
    return layers;
}

void Ann::predict(float *_x, float* _out_buffer){
    auto forward = Ann::feedforward(_x);
    auto last_layer = forward[forward.size()-1];
    last_layer.to_array(_out_buffer);
}

Matrix<float> Ann::mean_squared_error(Matrix<float> _target, Matrix<float> _output, bool drv){
    Matrix<float> _res(_target.get_height(), _target.get_width());
    for(int i = 0 ; i < _target.get_height() ; i++){
        for(int j = 0 ; j < _target.get_width() ; j++){
            if(!drv){
                _res.mat[i][j] = 0.5 * pow(_output.mat[i][j] -_target.mat[i][j],2);
            }else{
                _res.mat[i][j] = _output.mat[i][j] -_target.mat[i][j];
            }
        }
    }
    return _res;
}

float Ann::fit(float *_x, float *_y){
    auto ann_out = feedforward(_x);
    auto target_mat = Matrix<float>::from_array(_y, output_num);
    auto input_mat = Matrix<float>::from_array(_x, input_num);
    auto net_sum_error = mean_squared_error(target_mat, ann_out[ann_out.size()-1], false).sum();
    vector<Matrix<float>> delta;
    auto last_layer_delta = mean_squared_error(target_mat, ann_out[ann_out.size()-1], true) *
        Matrix<float>::map(ann_out[ann_out.size()-2], this->drv_tanh);
    delta.push_back(last_layer_delta);
    int delta_count = 0;
    for(int i = weights.size()-1 ; i >= 1 ; i--){
        auto layer_raw = Matrix<float>::transpose(weights[i]) * delta[delta_count];
        auto drv_a = Matrix<float>::map(ann_out[i*2],this->drv_tanh);
        auto layer_data = layer_raw * drv_a;
        delta.push_back(layer_data);
        delta_count++;
    }
    vector<Matrix<float>> weight_delta;
    delta_count = 0;
    for(int i = weights.size()-1; i >= 1 ; i--){
        auto w_delta = delta[delta_count] * Matrix<float>::transpose(ann_out[((i-1)*2)+1]);
        weight_delta.push_back(w_delta);
        delta_count++;
    }
    auto w_1_delta = delta[delta.size()-1] * Matrix<float>::transpose(input_mat);
    weight_delta.push_back(w_1_delta);
    reverse(weight_delta.begin(), weight_delta.end());
    
    for(int i = 0 ; i < weight_delta.size() ; i++){
        weight_delta[i] = weight_delta[i] * this->lr;
        weights[i] = weights[i] - weight_delta[i];
    }

    vector<Matrix<float>> bias_delta;
    for(int i = 0 ; i < bias.size() ; i++){
        auto b_delta = delta[delta.size()-1-i] * this->lr;
        bias_delta.push_back(b_delta);
    }

    for(int i = 0 ; i < bias.size() ; i++){
        bias[i] = bias[i] - bias_delta[i];
    }

    return net_sum_error;
}