#include <iostream>
#include "../include/Ann.hpp"

using namespace std;

int main(){
    
    float x[4][2] = {
        {0,0},
        {0,1},
        {1,0},
        {1,1}
    };
    float y[4][3] = {
        {0},
        {1},
        {1},
        {0}
    };

    int arch[] = {2,5,1};
    Ann ann(arch, sizeof(arch)/sizeof(*arch), 0.01);
    // ann.fit(x[0],y[0]);
    for(int epoch = 0 ; epoch < 500 ; epoch++){
        float err=0;
        for(int i = 0 ; i < 4 ; i++){
            err+=ann.fit(x[i],y[i]);
        }
        cout<<"Error : "<<err<<endl;
    }
    cout<<"----------------"<<endl;
    for(int i = 0 ; i < 4 ; i++){
        float res[1];
        ann.predict(x[i], res);
        cout<<x[i][0]<<","<<x[i][1]<<"="<<res[0]<<endl;
    }
    return 0;
}